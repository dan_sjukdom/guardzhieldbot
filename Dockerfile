FROM node:14.15.3-alpine3.12

EXPOSE 3030
ENV NODE_ENV dev


WORKDIR /app

COPY package.json .
COPY yarn.lock .

RUN yarn install

COPY . .

CMD ["nodemon", "index.js"]
