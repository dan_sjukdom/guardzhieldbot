Install postgis:
```shell
$ sudo apt install postgresql-13-postgis-3 postgresql-13-postgis-3-scripts
```


Create the database:
```shell
$ sudo -u postgres createdb gzbot
$ sudo -u postgres psql -d gzbot
```


Run the postgresql containers:
```
$ docker run -it -d --rm -e POSTGRES_PASSWORD=iamtylerdurden -e POSTGRES_USER=tylerdurden -e POSTGRES_DB=gzbot -p 5433:5432 -v postgres:/var/lib/postgresql/data --name postgres postgres:13.1
```

Connect to it:
```
$ docker exec -it postgres bash
# psql guardzhieldbot -h localhost -U tylerdurden
```

Optional (if there's a trouble using docker volumes) 
```
$ docker volume ls
$ docker volume prune
```