CREATE EXTENSION IF NOT EXISTS postgis;
CREATE EXTENSION IF NOT EXISTS postgis_topology;

-- GEOGRAPHY DATA TYPE IS PROVIDED BY POSTGIS EXTENSION
CREATE TABLE IF NOT EXISTS in_location (
    id SERIAL PRIMARY KEY,
    coordinates GEOGRAPHY(POINT) NOT NULL
);


CREATE TABLE IF NOT EXISTS incidents (
    id SERIAL PRIMARY KEY,
    in_description TEXT NOT NULL,
    in_direction TEXT NULL,
    in_location INT,
    CHECK (in_description != ''),
    CHECK (in_direction != ''),
    CONSTRAINT fk_in_location
        FOREIGN KEY(in_location)
            REFERENCES in_location(id)
            ON DELETE CASCADE
);


CREATE USER gzbotuzer WITH ENCRYPTED PASSWORD 'iamthevioletflame';

GRANT ALL ON DATABASE gzbot TO gzbotuzer;

-- INSERT INTO in_location (coordinates) VALUES ('POINT(55.4 -44.34)')