import { Router } from "express";
import { IncidentAPI } from "../controllers/incidents";


const router = Router();


router.get("/", IncidentAPI);


export default router;