import { Router } from "express";
import { Address2Coord } from "../controllers/mapquest";


const router = Router();


router.get("/get_coordinates/", Address2Coord);


export default router;
