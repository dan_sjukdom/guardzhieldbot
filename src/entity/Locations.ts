import {
    PrimaryGeneratedColumn,
    Entity,
    Column,
    OneToMany
} from "typeorm";
import { Geometry } from "geojson";
import { Incident } from "./Incidents";


@Entity({ name: "locations" })
export class Location {
    @PrimaryGeneratedColumn()
    id: number;

    @OneToMany(() => Incident, incident => incident.location)
    incidents: Location[];

    @Column({ type: "geometry", nullable: false })
    coordinates: Geometry;
};