import { 
    Entity,
    PrimaryGeneratedColumn,
    Column,
    ManyToOne,
    CreateDateColumn
} from "typeorm";
import { Location } from "./Locations";


@Entity({ name: "incidents" })
export class Incident {
    @PrimaryGeneratedColumn()
    id: number;

    @ManyToOne(() => Location, location => location.incidents)
    location: Location;

    @Column({ type: "text", nullable: false })
    description: string;

    @Column({ type: "text", nullable: true })
    direction: string;
    
    @CreateDateColumn()
    created_at: Date;
    
};