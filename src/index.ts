import express from "express";
import bodyParser from "body-parser";
import morgan from "morgan";
import helmet from "helmet";
import IncidentsRouter from "./routes/incidents.routes";
import MapquestRouter from "./routes/mapquest.routes";
import cors from "cors";


const app = express();


app.use(bodyParser({extended: false}))
app.use(bodyParser.json());
app.use(morgan("common"));
app.use(cors({
    origin: "http://localhost:3000"
}));
app.use(helmet());

app.use("/api/incidents", IncidentsRouter);
app.use("/api/mapquest", MapquestRouter);


const PORT = process.env.PORT || 3030;


app.listen(PORT, () => {
    console.log(`Listening on ${PORT}`);
}); 