import axios from "axios";
import {
    Response,
    Request
} from "express";


process.env.NODE_ENV !== "production" ? require("dotenv").config() : null;

const MAPQUEST_KEY = process.env.MAPQUEST_KEY;


export const Address2Coord = async (req: Request, res: Response) => {
    try {
        const location = req.query.location;
        const base_url = `http://www.mapquestapi.com/geocoding/v1/address?key=${MAPQUEST_KEY}&location=${location}&maxResults=3`;
        const response = await axios.get(base_url);
        if (response.status === 200) {
            const data = response.data;
            const location = data.results[0].locations.filter(location => location.adminArea1 === "MX");
            if (location.length > 0 ) {
                const coordinates = {
                    longitude: location[0].latLng.lat,
                    latitude: location[0].latLng.lng,
                }
                res.json(coordinates);
            }
            else {
                res.json({
                    message: "No hay datos disponibles para la dirección especificada"
                });
            }
        }
        else {
            res.status(400).json({
                message: "There was an error making the request"
            });
        }
    }
    catch (err) {
        res.status(400).json({
            message: err.message
        });
    }
};
